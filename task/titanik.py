import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


titanic_df = get_titatic_dataframe()
title_median_mapping = {
    'Mr.': None,
    'Mrs.': None,
    'Miss.': None
}

# Calculate median age for each title
for title in title_median_mapping:
    title_median_mapping[title] = round(titanic_df[titanic_df['Name'].str.contains(title)]['Age'].median())

# Find the number of missing values and calculate the median values
result = []
for title in title_median_mapping:
    missing_values = titanic_df[(titanic_df['Name'].str.contains(title)) & (titanic_df['Age'].isnull())].shape[0]
    median_value = title_median_mapping[title]
    result.append((title, missing_values, median_value))

# Print the result
print(result)