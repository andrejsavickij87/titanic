import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    title_median_mapping = {
    'Mr.': None,
    'Mrs.': None,
    'Miss.': None
}


for title in title_median_mapping:
    title_median_mapping[title] = round(df[df['Name'].str.contains(title)]['Age'].median())

result = []
for title in title_median_mapping:
    missing_values = df[(df['Name'].str.contains(title)) & (df['Age'].isnull())].shape[0]
    median_value = title_median_mapping[title]
    result.append((title, missing_values, median_value))
    return result
return result

# Print the result
print(result)
